import Jogador from "./classes/jogador.js";
import Time from "./classes/time.js";
import Jogo from "./classes/jogo.js";
import Estadio from "./classes/estadio.js";
import Tecnico from "./classes/tecnico.js";
import Arbitro from "./classes/arbitro.js"
import Participacoes from "./classes/participacoes.js";


let time = new Time("TimeFulanisons", true, 107,"Bahiano da Costa",12,nomeTecnico);
let jogador = new Jogador("Gilberto Bernardo", 46, nomeTime);
let jogo = new Jogo(3,new Date(),nomeEstadio);
let estadio = new Estadio("Estádio do Piolho", "São José Coutinho");
let tecnico = new Tecnico("Kalumbo Souza", nomeTime);
let arbitro = new Arbitro("José dos Santos", ["amarelo", "vermelho"],true);
let participacoes = new Participacoes()